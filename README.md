# README #

author: Adam Książek

### Temat ###

Tematem zadania jest stworzenie formularza kontaktowego, z którego zgłoszenia będą zapisywane
w bazie danych i wysyłane na wskazany adres e-mail.

### Pola Formularza: ###
Pola formularza:<br />
• imię<br />
• nazwisko<br />
• telefon kontaktowy<br />
• adres e-mail<br />
• treść wiadomości<br />

### Wymagania ###
Wymagania dotyczące walidacji danych:<br />
• muszą być wypełnione pola imię, nazwisko i treść wiadomości oraz przynajmniej jedno z pól
telefon kontaktowy/adres e-mail<br />
• wartości w polach imię i nazwisko nie mogą być dłuższe niż 40 znaków, natomiast treść
wiadomości 400 znaków<br />
• pole telefon kontaktowy może przyjmować wartości w formatach xxxxxxxxx, xxx-xxx-xxx
oraz xx-xxx-xx-xx (cyfry mogą być oddzielone myślnikami lub odstępami)<br />

### Baza danych ORM Doctrine ###
Aby utworzyć bazę danych wystarczy skorzytać z możliwości Doctrine,
aby wszystko wykonało się automatycznie, na bazie, która jest ustawiona w 
pliku app/config/parameters.yml
<br />

