<?php

namespace AppBundle\Services\ContactForm\WriteToDatabase;


use AppBundle\Entity\Contact\Form;
use AppBundle\Services\Misc\ResolverIpClient;
use AppBundle\Utils\Contact\ContactModelUtil;
use AppBundle\Utils\Contact\EmailModelUtil;
use Doctrine\ORM\EntityManagerInterface;

class WriteToDatabaseService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ResolverIpClient
     */
    private $resolverIpClient;

    /**
     * WriteToDatabaseService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ResolverIpClient $resolverIpClient
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ResolverIpClient $resolverIpClient
    ) {
        $this->entityManager = $entityManager;
        $this->resolverIpClient = $resolverIpClient;
    }

    /**
     * @param EmailModelUtil $emailModelUtil
     * @param ContactModelUtil $contactModelUtil
     * @return Form
     */
    public function writeToDatabase(EmailModelUtil $emailModelUtil, ContactModelUtil $contactModelUtil)
    {
        $formContact = new Form();
        $formContact->setFirstname($contactModelUtil->getFirstname());
        $formContact->setLastname($contactModelUtil->getLastname());
        $formContact->setPhone($contactModelUtil->getPhone());
        $formContact->setEmail($contactModelUtil->getEmail());
        $formContact->setComment($contactModelUtil->getComment());
        $formContact->setCreatedAt($contactModelUtil->getCreatedAt());
        $formContact->setIp($this->resolverIpClient->getIPAddress());

        $this->entityManager->persist($formContact);
        $this->entityManager->flush();

        return $formContact;
    }
}