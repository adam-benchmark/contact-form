<?php

namespace AppBundle\Services\ContactForm\Email;

use AppBundle\Utils\Contact\ContactModelUtil;
use AppBundle\Utils\Contact\EmailModelUtil;

interface EmailRepositoryInterface
{
    public function sendEmail(EmailModelUtil $emailModelUtil, ContactModelUtil $contactModelUtil);
}