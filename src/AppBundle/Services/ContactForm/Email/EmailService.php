<?php

namespace AppBundle\Services\ContactForm\Email;


use AppBundle\Utils\Contact\ContactModelUtil;
use AppBundle\Utils\Contact\EmailModelUtil;

class EmailService
{
    const TITLE = 'Thank You for contact';

    /**
     * @var EmailRepositoryInterface
     */
    private $email;

    /**
     * EmailService constructor.
     * @param EmailRepositoryInterface $email
     */
    public function __construct(EmailRepositoryInterface $email)
    {
        $this->email = $email;
    }

    /**
     * @param EmailModelUtil $emailModelUtil
     * @param ContactModelUtil $contactModelUtil
     */
    public function send(EmailModelUtil $emailModelUtil, ContactModelUtil $contactModelUtil)
    {
        $this->email->sendEmail($emailModelUtil, $contactModelUtil);
    }
}