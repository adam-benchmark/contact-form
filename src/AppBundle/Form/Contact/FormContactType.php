<?php

namespace AppBundle\Form\Contact;


use AppBundle\Validator\Constraints\IsEmail;
use AppBundle\Validator\Constraints\IsPhone;
use AppBundle\Validator\Constraints\IsPhoneValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class FormContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'First Name',
                'required' => true,
                'attr' => ['maxlength' => 40]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Last Name',
                'required' => true,
                'attr' => ['maxlength' => 40]
            ])
            ->add('phone', TextType::class, [
                'label' => 'phone',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new NotNull(),
                    new Length(
                        [
                            'min' => 6,
                        ]
                    ),
                    new IsPhone()
                ]
            ])
            ->add('email', TextType::class, [
                'label' => 'email',
                'required' => true,
                'constraints' => [
                    new IsEmail()
                ]
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Comment',
                'required' => true,
                'attr' => [
                    'maxlength' => 400,
                    'rows' => '6'
                ]
            ])
            /*->add('created_at', DateTime::class, [
                'attr' => ['style'=>'display:none;']
            ])*/
            ;
        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'submit',
                'attr' => [
                    'style' => 'margin-top: 10px',
                    'class' => 'btn btn-outline btn-xl js-scroll-trigger',
                ]

            ]);
    }

}