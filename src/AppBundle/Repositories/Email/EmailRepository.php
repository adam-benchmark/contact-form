<?php

namespace AppBundle\Repositories\Email;

use AppBundle\Services\ContactForm\Email\EmailRepositoryInterface;
use AppBundle\Utils\Contact\ContactModelUtil;
use AppBundle\Utils\Contact\EmailModelUtil;
use Swift_Mailer;
use Twig_Environment;

class EmailRepository implements EmailRepositoryInterface
{
    /** @var Twig_Environment  */
    protected $twig;
    /** @var Swift_Mailer  */
    protected $mailer;

    /**
     * EmailRepository constructor.
     * @param Twig_Environment $twig
     * @param Swift_Mailer $mailer
     */
    public function __construct(Twig_Environment $twig, Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }

    /**
     * @param EmailModelUtil $emailModelUtil
     * @param ContactModelUtil $contactModelUtil
     */
    public function sendEmail(EmailModelUtil $emailModelUtil, ContactModelUtil $contactModelUtil)
    {
        $message = (new \Swift_Message($emailModelUtil->getTitle()))
            ->setFrom($emailModelUtil->getFrom())
            ->setTo($emailModelUtil->getTo())
            ->setBody(
                $this->twig->render(
                    '@App/EmailTemplate/email_template.html.twig',
                    [
                        'contact' => $contactModelUtil
                    ]
                ),
                'text/html'
            );

        $this->mailer->send($message);
    }
}