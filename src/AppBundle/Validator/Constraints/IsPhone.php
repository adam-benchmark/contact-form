<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsPhone extends Constraint
{
    public $message = 'phone_is_invalid';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}