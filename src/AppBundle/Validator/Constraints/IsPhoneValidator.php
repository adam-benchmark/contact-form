<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsPhoneValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^[+0-9. ()-]*$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}