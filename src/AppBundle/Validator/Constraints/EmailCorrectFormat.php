<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EmailCorrectFormat
 * @package App\MainBundle\Validator\Constraints
 */
class EmailCorrectFormat extends Constraint
{
    /**
     * @var string
     */
    public $message = 'email_is_invalid';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}