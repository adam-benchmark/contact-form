<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsEmailUnique extends Constraint
{
    public $message = 'email_is_invalid';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}