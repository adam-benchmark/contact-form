<?php

namespace AppBundle\Controller\Contact;

use AppBundle\Controller\BaseController;
use AppBundle\Form\Contact\FormContactType;
use AppBundle\Services\ContactForm\Email\EmailService;
use AppBundle\Utils\Contact\ContactModelUtil;
use AppBundle\Utils\Contact\EmailModelUtil;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends BaseController
{
    const COOKIE_BLOCK_SEND_NEXT_FORM = 'blockSendNextForm';
    const SORRY_CANNOT_SENT_FORM = 'Very sorry, You\'ve sent Your form already. Must to wait more than 1 minute';

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $contactModelUtil = new ContactModelUtil();

        $form = $this->createForm(FormContactType::class, $contactModelUtil);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $contactModelUtil = $this->buildContactModelUtil($data);
            $emailModelUtil = $this->buildEmailModelUtil($data);

            if ($this->isCookieWhichBlockSendForm($request)) {
                return $this->redirectToRoute('contact_congratulations', [
                    'message' => self::SORRY_CANNOT_SENT_FORM
                ]);
            }

            $this->get('email.send')->send($emailModelUtil, $contactModelUtil);
            $this->get('contact_to_database')->writeToDatabase($emailModelUtil, $contactModelUtil);

            $this->setCookieToBlockNextFormSend();

            return $this->redirectToRoute('contact_congratulations');
        }

        return $this->render('AppBundle:Contact:form.html.twig', [
            'title' => 'Contact Form',
            'form' => $form->createView()
        ]);
    }

    /**
     * @param ContactModelUtil $contactModelUtil
     * @return ContactModelUtil
     */
    private function buildContactModelUtil(ContactModelUtil $contactModelUtil)
    {
        $contactModelUtil->setCreatedAt(new \DateTime());

        return $contactModelUtil;
    }

    /**
     * @param ContactModelUtil $contactModelUtil
     * @return EmailModelUtil
     */
    private function buildEmailModelUtil(ContactModelUtil $contactModelUtil)
    {
        $emailModelUtil = new EmailModelUtil();
        $emailModelUtil->setTitle(EmailService::TITLE);
        $emailModelUtil->setTo($contactModelUtil->getEmail());
        $emailModelUtil->setFrom($this->container->getParameter('mailer_user'));

        return $emailModelUtil;
    }

    private function setCookieToBlockNextFormSend()
    {
        $response = new Response();
        $cookie = new Cookie(self::COOKIE_BLOCK_SEND_NEXT_FORM, '1', time() + 60);
        $response->headers->setCookie($cookie);
        $response->send();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function isCookieWhichBlockSendForm(Request $request)
    {
        if ($request->cookies->get(self::COOKIE_BLOCK_SEND_NEXT_FORM)) {

            return true;
        }

        return false;
    }
}