<?php

namespace AppBundle\Controller\Contact;

use AppBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;

class CongratulationController extends BaseController
{
    const CONGRATULATIONS_TEXT = 'Dziękujemy za wysłanie wiadomości';
    const MESSAGE_KEY_NAME = 'message';

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $message = '';
        /*if (!empty($request->get(self::MESSAGE_KEY_NAME)) {
            $message = $request->get('message');
        }*/

        return $this->render('@App/Contact/congratulations.html.twig', [
            'congratulations_text' => self::CONGRATULATIONS_TEXT,
            'message' => !empty($request->get(self::MESSAGE_KEY_NAME)) ? $request->get(self::MESSAGE_KEY_NAME) : ''
        ]);
    }
}