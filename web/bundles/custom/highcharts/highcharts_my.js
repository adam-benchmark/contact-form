$(function () {
    $('.pie-chart-highchart').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Information about Source of Social Channels for Order Confirmation'
        },
        subtitle: {
            text: 'Chart'
        },
        plotOptions: {
            pie: {
                innerSize: 150,
                depth: 30
            }
        },
        series: [{
            name: 'Delivered amount',
            data: [
                ['Facebook', 3],
                ['Google', 1],
                ['Magazine', 0],
                ['Press', 0],
                ['Poster', 1],
                ['Radio', 0],
                ['Friends', 0],
                ['Others', 0]
            ]
        }]
    });
});
