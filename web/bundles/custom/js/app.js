$(function () {

    $('body').on('submit', '.ajaxForm', function () {
        var form = $(this);

        var options = {
            beforeSend: function (e) {
                form.find('input, button, textarea, select').prop('disabled', true);

                form.find('ul.form-errors').remove();
            },
            complete: function (response) {
                var response = response.responseJSON;
                form.find('input, button, textarea, select').prop('disabled', false);

                if (!response.success) {
                    for (var key in response.errors) {
                        var errStr = '<ul class="form-errors">';
                        for (var i in response.errors[key]) {
                            errStr += '<li>' + response.errors[key][i] + '</li>';
                        }
                        errStr += '</ul>';
                        form.find('#' + response.form_prefix + '_' + key).after(errStr);
                    }
                }

                if (typeof response.scripts != 'undefined') {
                    for (var k in response.scripts) {
                        window[k](response.scripts[k][0], response.scripts[k][1], response.scripts[k][2], response.scripts[k][3], response.scripts[k][4], response.scripts[k][5]);
                    }
                }
            }
        };

        $(this).ajaxSubmit(options);

        return false;
    });

    $('body').on('click', '[data-destination]:not(.ignore-ajax)', function () {
        var destination = $(this).attr('data-destination');
        var url = $(this).attr('href');

        loadUrlToDestination(url, destination);

        return false;
    });

    $('.multiselectable').each(function () {
        if ($(this).hasClass('select_all')) {
            var multiselect_options = {
                includeSelectAllOption: true,
                enableFiltering: true
            };
        } else {
            var multiselect_options = {
                enableFiltering: true
            };
        }

        $(this).multiselect(multiselect_options);
    });

    $('.modal').on('shown.bs.modal', function () {
        $(this).removeData('bs.modal');
    });

    $('a[data-confirm]').click(function (ev) {
        var href = $(this).attr('href');
        $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
        $('#dataConfirmOK').attr('href', href);
        $('#dataConfirmModal').modal({show: true});
        return false;
    });

    $('.nav > .nav-header').click(function () {
        $(this).nextUntil(".nav-header").slideDown(300);
    });

    $('.datepicker-standard').datepicker({
        format: 'yyyy-mm-dd'
    });

    handleAnimations();

    $(window).bind("scroll", function () {
        handleAnimations();
    });

    $('.bs-component > .alert').each(function () {
        var text = $(this).find('.text').html();

        if ($(this).hasClass('alert-danger')) {
            text = '<div class="danger notification-item">' + text + '</div>';
        } else if ($(this).hasClass('alert-success')) {
            text = '<div class="success notification-item">' + text + '</div>';
        } else if ($(this).hasClass('alert-warning')) {
            text = '<div class="warning notification-item">' + text + '</div>';
        }

        text = '<div class="notification-alert">' + text + '</div>';

        pushNotification(text);
    });

    $('#side-modal-container .layer').click(function () {
        hideSideModal();
    });

    $(document).on('click', '#login-as-customer', function (event) {
        event.preventDefault();
        var $self = $(this);
        var customerId = $self.data('customer-id');
        var $loginDomain = $('#login-domain');
        var createTokenUrl = $self.data('url');
        if($loginDomain.length && createTokenUrl) {
            var loginDomain = $loginDomain.val();
            if(loginDomain) {
                $.post(createTokenUrl, {
                    'id_customer' : customerId,
                        'login_domain' : loginDomain
                }, function (response) {
                    if(response && response.success && response.redirectUrl && response.loginDomain) {
                        var authorizeUrl = response.loginDomain + response.redirectUrl;
                        var win = window.open(authorizeUrl, '_blank');
                        console.log("Login url for customer:", authorizeUrl);
                        if (win)
                            win.focus();
                        else
                            flashMessage('error', 'Please allow popups for this website');
                    } else
                        flashMessage('error', 'Problem: login as customer fail');
                });
            }
        }
    });
});