$(document).ready(function () {
    $('#image_type_regenerate').click(function () {
        var originalUrl = $(this).attr('href');
        var id_image_type = $('#id_image_type').val();
        var cursor = 0;

        $('#image_type_regenerate_box').hide();
        $('#image_type_regenerate_notify').show();

        regeneratePiece(originalUrl, id_image_type, cursor);

        return false;
    });
});

function regeneratePiece(originalUrl, id_image_type, cursor)
{
    url = originalUrl.replace('id_image_type', id_image_type).replace('cursor', cursor);

    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        dataType: "json",
        success: function (jsonData) {
            if (jsonData.result) {
                $('#image_type_regenerate_box').show();
                $('#image_type_regenerate_notify').hide();
            } else {
                $('#image_type_regenerate_counter').text(jsonData.count);
                regeneratePiece(originalUrl, id_image_type, jsonData.cursor);
            }
        },
    });
}