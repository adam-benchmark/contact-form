function animateElement(element, effect, onend) {
    element.addClass(effect + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        element.removeClass(effect).removeClass('animated');
        try {
            onend();
        } catch (e) {
        }
    });
}

function handleAnimations() {
    $("[data-scroll-animation]:not(.already-animated)").withinviewport().each(function () {
        if ($(this).hasClass('hide')) {
            $(this).removeClass('hide');
        }
        if ($(this).hasClass('invisible')) {
            $(this).removeClass('invisible');
        }

        animateElement($(this), $(this).attr('data-scroll-animation'));
        $(this).addClass('already-animated');
    });
}

function pushNotification(html, displayTime) {
    var id = Math.floor(Math.random() * 1000000);
    $('#notifications').prepend('<div class="' + id + '">' + html + '</div>');
    animateElement($('.' + id), 'fadeInRight', function() {
        animateElement($('.' + id), 'pulse');
    });

    if(displayTime == undefined)
        displayTime = 5000;

    setTimeout(function () {
        animateElement($('.' + id), 'fadeOutRight', function () {
            $('.' + id).remove();
        });
    }, displayTime);
}

function redirect(url) {
    location.href = url;
}

function flashMessage(type, text) {
    if (type == 'error') {
        text = '<div class="danger notification-item">' + text + '</div>';
    } else if (type == 'success') {
        text = '<div class="success notification-item">' + text + '</div>';
    } else if (type == 'warning') {
        text = '<div class="warning notification-item">' + text + '</div>';
    }

    text = '<div class="notification-alert">' + text + '</div>';

    pushNotification(text);
}

function showPreloader() {
    $('#preloader').show();
}

function hidePreloader() {
    $('#preloader').hide();
}

function showSideModal() {
    $('#side-modal-container').show();
    animateElement($('#side-modal-container'), 'fadeIn');

    $('#side-modal-container .panel').show();
    animateElement($('#side-modal-container .panel'), 'fadeInRight');
}

function hideSideModal() {
    animateElement($('#side-modal-container'), 'fadeOut', function() {
        $('#side-modal-container').hide();
    });

    animateElement($('#side-modal-container .panel'), 'fadeOutRight', function() {
        $('#side-modal-container .panel').hide();
    });
}

function loadToSideModal(html,showSideModel) {
    $('#side-modal-container .panel').html(html);

    if(showSideModel === false ) return;
    if($('#side-modal-container').css('display') != 'block') {
        showSideModal();
    }
}

function loadUrlToDestination(url, destination, onend) {
    showPreloader();
    $(destination).html('');

    $.get(url, function (response) {
        $(destination).html(response);
        animateElement($(destination), 'fadeIn');
        hidePreloader();

        try {
            onend();
        } catch (e) {
        }
    }).fail(function () {
        swal('Oops', 'error', 'error');
        hidePreloader();
        $(destination).html('');
    });
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}