$(document).ready(function () {
    $('#product_quality_check').click(function () {
        var originalUrl = $(this).attr('href');
        var cursor = 0;

        $('#product_quality_check_box').hide();
        $('#product_quality_check_notify').show();

        regeneratePiece(originalUrl, cursor);

        return false;
    });
});

function regeneratePiece(originalUrl, cursor)
{
    url = originalUrl.replace('cursor', cursor);

    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        dataType: "json",
        success: function (jsonData) {
            if (jsonData.result) {
                $('#product_quality_check_box').show();
                $('#product_quality_check_notify').hide();
                displayLogs(jsonData.log);
            } else {
                $('#product_quality_check_counter').text(jsonData.count);
                displayLogs(jsonData.log);
                regeneratePiece(originalUrl, jsonData.cursor);
            }
        },
    });
}

function displayLogs(logs)
{
    $.each(logs, function (key, log) {
        var logLine = '' +
            '<tr>' +
            '<td class="vert-align">'+log.id+'</td>' +
            '<td class="vert-align">'+log.action+'</td>' +
            '<td class="vert-align">'+log.shop+'</td>' +
            '<td class="vert-align">'+log.reason+'</td>' +
            '<td class="vert-align"></td>' +
            '<td>'+(log.link ? '<a class="btn btn-lg btn-danger-outline m-t-xs" href="'+log.link+'">edit</a>' : '')+'</td>';
        $('.table-full table.table tbody').append(logLine);
    })
}